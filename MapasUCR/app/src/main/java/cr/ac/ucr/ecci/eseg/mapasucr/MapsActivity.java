package cr.ac.ucr.ecci.eseg.mapasucr;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.location.Location;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.tasks.OnSuccessListener;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.FusedLocationProviderClient;



public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {
    private GoogleMap mMap;
    private Boolean controlsEnabled = false;
    private FusedLocationProviderClient fusedLocationClient;
    private static final int LOCATION_REQUEST_CODE = 101;

    @Override
    public void onMapClick(LatLng latLng) {
// cuando se hace clic sobre el mapa se muestran las coordenadas
// nos movemos a la posicion donde hizo clic
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
// mostramos las coordenadas
        Toast.makeText(getApplicationContext(), latLng.toString(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void onMapLongClick(LatLng latLng) {
// Agregamos un marcador cuando el usuario deja presionado un punto en el mapa
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title(latLng.toString())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
// Mensaje
        Toast.makeText(getApplicationContext(), "Nuevo marcador: " + latLng.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
// Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
    }


    private void obtenerLocalizacion() {
// Solo si mapa está instanciado
        if (mMap == null) return;
// Revisar permisos
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
            return;
// Tratar de obtener localización
// Mediante proceso asincrónico
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if(location != null) {
// Posicionar cámara en posición actual
                            CameraPosition cameraPosition = new CameraPosition.Builder()
                                    .target(new LatLng(location.getLatitude(),
                                            location.getLongitude()))
                                    .zoom(20)
                                    .bearing(70)
                                    .tilt(25)
                                    .build();
                            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        }
                    }
                });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull
            int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED) {
// Se otorga
                    if (ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mMap.setMyLocationEnabled(true);
                }
            }
        }
    }

    protected void solicitarPermiso(String permissionType, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permissionType}, requestCode);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        } else {
            solicitarPermiso(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_REQUEST_CODE);
        }

 LatLng sodaOdontologia = new LatLng(9.938035, -84.051509);
 mMap.addMarker(new MarkerOptions().position(sodaOdontologia).title("Antigua Soda de Odontología de la UCR"));
 mMap.moveCamera(CameraUpdateFactory.newLatLng(sodaOdontologia));
        // Asignamos el evento de clic en el mapa
        mMap.setOnMapClickListener(this);
// Asignamos el evento de clic largo en el mapa
        mMap.setOnMapLongClickListener(this);

    }




    @Override
// Creamos el menu de opciones
    public boolean onCreateOptionsMenu(Menu menu) {
// Inflate the menu: this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
// Implementamos las llamadas al menú de opciones
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_1) {
// Marcador soda UCR
            marcadorSodaUCR();
            return true;
        } else if (id == R.id.action_2) {
// Mostrar las opciones de Zoom
            mostrarZoom();
            return true;
        } else if (id == R.id.action_3) {
// Mover la posicion de la camara y hacer Zoom
            posicionCamaraZoom();
            return true;
        } else if (id == R.id.action_4) {
// Cambiar el tipo de mapa
            mapaSatelite();
            return true;
        } else if (id == R.id.action_5) {
// Cambiar el tipo de mapa
            mapaHibrido();
            return true;
        } else if (id == R.id.action_6) {
// Cambiar el tipo de mapa
            mapaNinguno();
            return true;
        } else if (id == R.id.action_7) {
// Cambiar el tipo de mapa
            mapaNormal();
            return true;
        } else if (id == R.id.action_8) {
// Cambiar el tipo de mapa
            mapaTierra();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void marcadorSodaUCR() {
// Agregar un marcador en la antigua soda de odontología
        LatLng sodaOdontologia = new LatLng(9.938035,-84.051509);
// Agregar marcador
        mMap.addMarker(new MarkerOptions().position(sodaOdontologia).title("Antigua Soda de Odontología de la UCR"));
// Mover la "cámara" del mapa hacia el marcador
                mMap.moveCamera( CameraUpdateFactory.newLatLng(sodaOdontologia) );
    }
    public void mostrarZoom() {
        controlsEnabled = !controlsEnabled;
        UiSettings mapSettings = mMap.getUiSettings();
        mapSettings.setZoomControlsEnabled(controlsEnabled);
        mapSettings.setCompassEnabled(controlsEnabled);
        mapSettings.setMyLocationButtonEnabled(controlsEnabled);
    }
    // Mover la cámara y hacer zoom
    public void posicionCamaraZoom() {
        LatLng sodaOdontologia = new LatLng(9.938035, -84.051509);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(sodaOdontologia)
                .zoom(20) // zoom level
                .bearing(70) // bearing // direccion de la camara
                .tilt(25) // tilt angle // inclinacion
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }
    public void mapaSatelite() { mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE); }
    public void mapaHibrido() { mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID); }
    public void mapaNinguno() { mMap.setMapType(GoogleMap.MAP_TYPE_NONE); }
    public void mapaNormal() { mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL); }
    public void mapaTierra() { mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN); }


}